#include <iostream>

using namespace std;

int	inputData();
double sumOne(int n);
double sumTwo(int n);
void testCasesForSumOne();
void userMode();

int main()
{
	cout << "Enter 1 - to run application in test mode" << endl;
	cout << "Enter 2 - to run application in user mode" << endl;
	cout << "Otherwise  - to exit" << endl;
	int mode;
	cin >> mode;
	system("cls");
	switch (mode)
	{
	case 1:
		testCasesForSumOne();
		break;
	case 2:
		userMode();
		break;
	default:
		return 0;
	}
}

void userMode()
{
	int n = inputData();
	cout << "1/1 + 1/2 + ... + 1/" << n << " = " << sumOne(n) << endl;
}

void testCasesForSumOne()
{
	cout << (abs(sumOne(6) - 2.4499999999999997) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumOne(10) - 2.9289682539682538) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumOne(100) - 5.187377517639621) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumOne(1000000) - 14.392726722864989) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumOne(10000000) - 16.695311365857272) <= 0 ? "Test passed" : "Test failed") << endl;
	system("pause");
}

int	inputData()
{
	int n;

	while (true)
	{
		cout << "Enter n > 0 : ";
		cin >> n;
		if (n > 0)
		{
			return n;
		}
		cout << "Invalid n. Try again." << endl;
		system("pause");
		system("cls");
	}
}

double sumOne(int n)
{
	double sum = 0;

	for (int i = 1; i <= n; i++)
	{
		sum += 1. / i;
	}

	return sum;
}

double sumTwo(int n)
{
	//TODO
	return 0;
}
